PDFParser - Test
================

> PDF doesn't have any concept of columns (heck, it doesn't even have a concept of words)


Folgende Frameworks sind einbezogen:
_________________________________
iText
-----
- <http://itextpdf.com/>
- seit 5.0 APGL <http://de.wikipedia.org/wiki/GNU_Affero_General_Public_License>
- Beispiele > <http://itextpdf.com/examples/iia.php?id=275>

PDFBox
------
- <http://pdfbox.apache.org>
- Apache 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
- Beispiele > <http://pdfbox.apache.org/1.8/cookbook/textextraction.html>
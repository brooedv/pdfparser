package test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @see 'http://pdfbox.apache.org/1.8/cookbook/textextraction.html'
 */
@Singleton
public class PDFBoxParser {

    @Inject
    private Log logger;

    public static void main(String[] args){
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        PDFBoxParser parser = container.instance().select(PDFBoxParser.class).get();
        parser.run();
        weld.shutdown();
    }

    public void run(){
        try {

            StringWriter stringWriter = new StringWriter();
            InputStream resourceAsStream = this.getClass().getResourceAsStream("/Ausstattung mit Codes.pdf");
            //InputStream resourceAsStream = this.getClass().getResourceAsStream("/Ausstattung ohne Codes.pdf");
            PDDocument pdDoc = PDDocument.load(resourceAsStream);

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setLineSeparator("\n");
            stripper.writeText(pdDoc, stringWriter);

            String text = stringWriter.toString();

            logger.info(text);

            stringWriter.close();
            pdDoc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}

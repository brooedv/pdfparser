package test;

import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import org.apache.commons.logging.Log;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @see 'http://itextpdf.com/examples/iia.php?id=275'
 */
@Singleton
public class ITextParser {

    @Inject
    private Log logger;

    public static void main(String[] args){
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        ITextParser parser = container.instance().select(ITextParser.class).get();
        parser.run();
        weld.shutdown();
    }

    public void run(){
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream("/Ausstattung mit Codes.pdf");
            //InputStream resourceAsStream = this.getClass().getResourceAsStream("/Ausstattung ohne Codes.pdf");
            PdfReader reader = new PdfReader(resourceAsStream);
            // we can inspect the syntax of the imported page
            byte[] streamBytes = reader.getPageContent(1);
            PRTokeniser tokenizer = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(streamBytes)));
            StringWriter out = new StringWriter();
            while (tokenizer.nextToken()) {
                if (tokenizer.getTokenType() == PRTokeniser.TokenType.STRING) {
                    out.write(tokenizer.getStringValue());
                    out.write("\n");
                }
            }
            out.flush();
            out.close();
            reader.close();

            logger.info(out.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
